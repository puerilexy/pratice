// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueLoadLazy from 'vue-lazyload'

Vue.filter()
Vue.config.productionTip = false
Vue.prototype.$http = axios

// Vue.use(VueLoadLazy) 可直接挂载
Vue.use(VueLoadLazy, {
    error: require('./assets/imgs/error.jpg'), // 加载失败
    loading: require('./assets/imgs/loading.gif') // 正在加载
})

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})
