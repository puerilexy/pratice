import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/drag'
        },
        {
            path: '/drag',
            name: 'drag',
            component: () => import('@/view/Drag')
        },
        {
            path: '/qrcode',
            name: 'qrcode',
            component: () => import('@/view/QRcode')
        },
        {
            path: '/debounce',
            name: 'debounce',
            component: () => import('@/view/debounce')
        },
        {
            path: '/lazyLoad',
            name: 'lazyLoad',
            component: () => import('@/view/lazyLoad')
        },
        {
            path: '/nextTick',
            name: 'nextTick',
            component: () => import('@/view/nextTick')
        },
        {
            path: '/baiduMap',
            name: 'baiduMap',
            component: () => import('@/view/baiduMap')
        },
        {
            path: '/vueFilter',
            name: 'vueFilter',
            component: () => import('@/view/vueFilter')
        }
    ],
    linkActiveClass: 'active'
})
