// 防抖
export function debounce (fn, delay) {
    let timeout = null;
    return function (args) {
        if (timeout) {
            clearTimeout(timeout)
        };
        setTimeout(() => {
            fn(args)
        }, delay)
    }
}
