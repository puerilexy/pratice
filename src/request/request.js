import axios from 'axios';

let axiosType = {
    axiosGet: (url, params) => {
        return axios.get(url, {params: params});
    }
}

export default axiosType;
